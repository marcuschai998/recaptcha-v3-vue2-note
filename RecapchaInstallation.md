# Recapcha

## Installation
```bash
npm install vue-recaptcha-v3@^1.9.0
```
## Usage
`.env` / `.env.example`
```
RECAPTCHA_SITE_KEY= <site key>
RECAPTCHA_SECRET= <secret key>
```

service.php config
```php
 'recaptcha' => [
        'key' => env('RECAPTCHA_SITE_KEY'),
        'secret' => env('RECAPTCHA_SECRET'),
    ],
```

main.js
```js
import Vue from 'vue'
import { VueReCaptcha } from 'vue-recaptcha-v3'

Vue.use(VueReCaptcha, {
	siteKey: '<site key>' ,
	loaderOptions: {
		autoHideBadge: true
	}
})
```

front page (example vue contactPage.vue)
```html
<v-btn
    @click="submitForm(form)"
>
    Submit
</v-btn>
```

```js
mounted(){
		this.initRecaptcha();
},

methods:{
		async initRecaptcha(){
			await this.$recaptchaLoaded()
		},
		async submitForm(item){
			// Execute reCAPTCHA with action "submit".
			const token = await this.$recaptcha('submit')
			this.form.recaptcha = token

			this.submitData(item);
		},

submitData(item){
    let payload = item
    this.loading = true;
    this.errors = {}

    BaseClient.sendMail(payload).then((res) => {
        this.$toast.success(res.data.message)
        let result = res.data.data
    }).catch((err) => {
       // error handling
    }).finally(()=>{
        this.loading = false;
    })
},

```
Validation
```php
  public function rules()
    {
        return [
            'recaptcha' => ['required', new Recaptcha()],
        ];
    }
```


### 
Recaptcha.php

```php
<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Http;
use Log;
class Recaptcha implements Rule
{
    const URL = 'https://www.google.com/recaptcha/api/siteverify';

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Http::post((static::URL)."?".http_build_query([
            'secret' => config('services.recaptcha.secret'),
            'response' => $value,
            'remoteip' => request()->ip()
        ]))->json()['success'];
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Recaptcha validation invalid';
    }
}


```